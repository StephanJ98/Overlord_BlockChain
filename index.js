require('dotenv').config()
const Block = require('./Block')
const Blockchain = require('./Blockchain')
const prompt = require('prompt-sync')({ sigint: true });

console.log("Overlord mining in progress....");

const addBlock = async (blockchain, sender, receiver, quantity) => {
    if (Number(quantity) >= 1) {
        await blockchain.addNewBlock(
            new Block(1, new Date().toISOString(), {
                sender: sender,
                recipient: receiver,
                quantity: Number(quantity)
            })
        )
    } else {
        console.log('Invalid quantity!')
    }
}

try {
    let response;
    let Overlord = new Blockchain(Number(process.env.DIFFICULTY));

    do {
        console.log('########################################################')
        console.log('Select option: ')
        console.log('-> Exit [N]')
        console.log('-> Add Block [A]')
        console.log('-> Show Blockchain [S]')
        console.log('-> Check the Validity [C]')
        console.log('########################################################')
        response = prompt('-> ');

        switch (response.toLocaleUpperCase()) {
            case 'A':
                console.log('Add block selected.')
                let sender = prompt('How´s the sender? ')
                let receiver = prompt('How´s the receiver? ')
                let quantity = prompt('How many tockens [=<1]? ')

                addBlock(Overlord, sender, receiver, quantity)
                break;

            case 'S':
                console.log(Overlord)
                break;

            case 'C':
                Overlord.checkChainValidity() == true ? console.log('The blockchain is ok') : console.log('The is a error in the blockchain')
                break;

            case 'N':
                break

            default:
                console.log('Invalid option')
                break;
        }

    } while (response.toLocaleUpperCase() !== 'N')

} catch (error) {
    console.error(error)
}